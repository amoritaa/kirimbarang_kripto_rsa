import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyB-CpkWTUKrm27XIsJMstjumfFGkIyS_r0",

  authDomain: "kirimbarang-75a16.firebaseapp.com",

  projectId: "kirimbarang-75a16",

  storageBucket: "kirimbarang-75a16.appspot.com",

  messagingSenderId: "220432585681",

  appId: "1:220432585681:web:c357680a2b1af372af86f0",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const projectFirestore = firebase.firestore();
export { projectFirestore };
