import { defineStore } from "pinia";
import { initializeApp } from "firebase/app";
import {
  getFirestore,
  collection,
  doc,
  setDoc,
  getDocs,
  onSnapshot,
  updateDoc,
  deleteDoc,
} from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyB-CpkWTUKrm27XIsJMstjumfFGkIyS_r0",

  authDomain: "kirimbarang-75a16.firebaseapp.com",

  projectId: "kirimbarang-75a16",

  storageBucket: "kirimbarang-75a16.appspot.com",

  messagingSenderId: "220432585681",

  appId: "1:220432585681:web:c357680a2b1af372af86f0",
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

// Function to encrypt using RSA
function encryptRSA(publicKey, message) {
  const n = publicKey.n;
  const e = publicKey.e;
  const charCodes = message.split("").map((char) => char.charCodeAt(0));
  const encryptedCharCodes = charCodes.map((charCode) => {
    let encryptedCharCode = 1;
    for (let i = 0; i < e; i++) {
      encryptedCharCode = (encryptedCharCode * charCode) % n;
    }
    return encryptedCharCode;
  });
  return encryptedCharCodes;
}

// Function to decrypt using RSA
function decryptRSA(privateKey, encryptedCharCodes) {
  const n = privateKey.n;
  const d = privateKey.d;
  const decryptedCharCodes = Array.from(encryptedCharCodes).map(
    (encryptedCharCode) => {
      let charCode = 1;
      for (let i = 0; i < d; i++) {
        charCode = (charCode * encryptedCharCode) % n;
      }
      return charCode;
    }
  );
  const message = decryptedCharCodes
    .map((charCode) => String.fromCharCode(charCode))
    .join("");
  return message;
}

export const useTodo = defineStore({
  id: "todo",
  state: () => ({
    category: [
      {
        id: 1,
        name: "Education",
      },
      {
        id: 2,
        name: "Shopping",
      },
      {
        id: 3,
        name: "Work",
      },
      {
        id: 4,
        name: "Funtime",
      },
    ],
    task: {
      newTask: "",
      nama: "",
      nomor: "",
      tujuan: "",
      penerima: "",
      newTodo: [],
    },
    edit: {
      editTask: "",
    },
  }),
  getters: {},
  actions: {
    async addNewTask() {
      let task = this.task;
      let id;
      if (task.newTodo.length == 0) {
        id = 0;
      }
      if (task.newTodo.length > 0) {
        for (let i = 0; i < task.newTodo.length; i++) {
          id = task.newTodo[i].id + 1;
        }
      }

      // Encrypt data using RSA
      const publicKey = { e: 5, n: 469 };
      const encryptedTask = encryptRSA(publicKey, task.newTask);
      const encryptedNama = encryptRSA(publicKey, task.nama);
      const encryptedNomor = encryptRSA(publicKey, task.nomor);
      const encryptedTujuan = encryptRSA(publicKey, task.tujuan);
      const encryptedPenerima = encryptRSA(publicKey, task.penerima);

      await setDoc(doc(db, "todo_list", id.toString()), {
        id: id,
        task: encryptedTask,
        nama: encryptedNama,
        nomor: encryptedNomor,
        tujuan: encryptedTujuan,
        penerima: encryptedPenerima,
        status: false,
      });
      task.newTask = "";
      task.nama = "";
      task.nomor = "";
      task.tujuan = "";
      task.penerima = "";
    },
    async getNewTasks() {
      let task = this.task;

      // Decrypt data using RSA
      const privateKey = { d: 317, n: 469 };
      onSnapshot(collection(db, "todo_list"), (querySnapshot) => {
        task.newTodo = [];
        querySnapshot.forEach((doc) => {
          const decryptedTask = decryptRSA(privateKey, doc.data().task);
          const decryptedNama = decryptRSA(privateKey, doc.data().nama);
          const decryptedNomor = decryptRSA(privateKey, doc.data().nomor);
          const decryptedTujuan = decryptRSA(privateKey, doc.data().tujuan);
          const decryptedPenerima = decryptRSA(privateKey, doc.data().penerima);
          task.newTodo.push({
            id: doc.data().id,
            task: decryptedTask,
            nama: decryptedNama,
            nomor: decryptedNomor,
            tujuan: decryptedTujuan,
            penerima: decryptedPenerima,
            status: doc.data().status,
          });
        });
      });
    },
    async updateStatus(id_task) {
      let task = this.task;
      let id = id_task;
      await updateDoc(doc(db, "todo_list", id.toString()), {
        status: !task.newTodo[id].status,
      });
    },
    async editTask(id_task) {
      let id = id_task;
      await updateDoc(doc(db, "todo_list", id.toString()), {
        task: this.task.newTodo[id].task,
      });
    },
    async deleteTask(id_task) {
      let id = id_task;
      await deleteDoc(doc(db, "todo_list", id.toString()));
    },
  },
});
