import { defineStore } from "pinia";
import { initializeApp } from "firebase/app";
import {
  getFirestore,
  collection,
  doc,
  setDoc,
  getDocs,
  onSnapshot,
  updateDoc,
  deleteDoc,
} from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyB-CpkWTUKrm27XIsJMstjumfFGkIyS_r0",

  authDomain: "kirimbarang-75a16.firebaseapp.com",

  projectId: "kirimbarang-75a16",

  storageBucket: "kirimbarang-75a16.appspot.com",

  messagingSenderId: "220432585681",

  appId: "1:220432585681:web:c357680a2b1af372af86f0",
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export const useTodo = defineStore({
  id: "todo",
  state: () => ({
    category: [
      {
        id: 1,
        name: "Education",
      },
      {
        id: 2,
        name: "Shopping",
      },
      {
        id: 3,
        name: "Work",
      },
      {
        id: 4,
        name: "Funtime",
      },
    ],
    task: {
      newTask: "",
      nama: "",
      nomor: "",
      tujuan: "",
      penerima: "",
      newTodo: [],
    },
    edit: {
      editTask: "",
    },
  }),
  getters: {},
  actions: {
    async addNewTask() {
      let task = this.task;
      let id;
      if (task.newTodo.length == 0) {
        id = 0;
      }
      if (task.newTodo.length > 0) {
        for (let i = 0; i < task.newTodo.length; i++) {
          id = task.newTodo[i].id + 1;
        }
      }
      await setDoc(doc(db, "todo_list", id.toString()), {
        id: id,
        task: task.newTask,
        nama: task.nama,
        nomor: task.nomor,
        tujuan: task.tujuan,
        penerima: task.penerima,
        status: false,
      });
      task.newTask = "";
      task.nama = "";
      task.nomor = "";
      task.tujuan = "";
      task.penerima = "";
    },
    async getNewTasks() {
      let task = this.task;
      // mendapatkan semua doc
      // const querySnapshot = await getDocs(collection(db, "todo_list"));
      // querySnapshot.forEach((doc) => {
      //   task.newTodo.push(doc.data());
      // });
      // mendapatkan hanya yg ditambahkan
      onSnapshot(collection(db, "todo_list"), (querySnapshot) => {
        task.newTodo = [];
        querySnapshot.forEach((doc) => {
          task.newTodo.push(doc.data());
        });
      });
    },
    async updateStatus(id_task) {
      let task = this.task;
      let id = id_task;
      await updateDoc(doc(db, "todo_list", id.toString()), {
        status: !task.newTodo[id].status,
      });
    },
    async editTask(id_task) {
      let id = id_task;
      await updateDoc(doc(db, "todo_list", id.toString()), {
        task: this.task.newTodo[id].task,
      });
    },

    async getEducation(categories) {
      let edu = this.edu;
      await getDocs(collection(db, "todo_list")).then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          if (doc.data().category.includes(categories)) {
            edu.push(doc.data());
          }
        });
      });
    },
    async getWork(categories) {
      let work = this.work;
      await getDocs(collection(db, "todo_list")).then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          if (doc.data().category.includes(categories)) {
            work.push(doc.data());
          }
        });
      });
    },
    async getShop(categories) {
      let shop = this.shop;
      await getDocs(collection(db, "todo_list")).then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          if (doc.data().category.includes(categories)) {
            shop.push(doc.data());
          }
        });
      });
    },
    async getFun(categories) {
      let fun = this.fun;
      await getDocs(collection(db, "todo_list")).then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          if (doc.data().category.includes(categories)) {
            fun.push(doc.data());
          }
        });
      });
    },
    async deleteTask(id_task) {
      let id = id_task;
      await deleteDoc(doc(db, "todo_list", id.toString()));
    },
  },
});
